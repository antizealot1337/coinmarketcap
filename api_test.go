package coinmarketcap

import (
	"net/http"
	"net/http/httptest"
	"net/url"
	"testing"
)

func TestCoinmarketcap(t *testing.T) {
	// Create values
	values := make(url.Values)

	// The urls
	expected := "https://api.coinmarketcap.com/v1/ticker"
	actual := coinmarketcap(values, "v1", "ticker")

	// Check the urls
	if actual != expected {
		t.Errorf(`Expected url to be "%s" but was "%s"`, expected, actual)
	} //if

	values.Add("a", "1")

	// The urls
	expected = "https://api.coinmarketcap.com/v1/ticker?a=1"
	actual = coinmarketcap(values, "v1", "ticker")

	// Check the urls
	if actual != expected {
		t.Errorf(`Expected url to be "%s" but was "%s"`, expected, actual)
	} //if
} //func

func TestMakeCurrenciesURL(t *testing.T) {
	// The expected values
	expected := url.URL{
		Scheme:   "https",
		Host:     "api.coinmarketcap.com",
		Path:     "/v1/ticker",
		RawQuery: "start=1&limit=2&currency=a",
	}

	// The actual url after parsing
	actual, err := url.Parse(makeCurrenciesURL(1, 2, "a"))

	// Check for an error
	if err != nil {
		t.Fatal("Unexpected error:", err)
	} //if

	// Check the Scheme
	if expected, actual := expected.Scheme, actual.Scheme; actual != expected {
		t.Errorf(`Expected scheme to be "%s" but was "%s"`, expected, actual)
	} //if

	// Check the host
	if expected, actual := expected.Host, actual.Host; actual != expected {
		t.Errorf(`Expected host to be "%s" but was "%s"`, expected, actual)
	} //if

	// Check the path
	if expected, actual := expected.Path, actual.Path; actual != expected {
		t.Errorf(`Expected path to be "%s" but was "%s"`, expected, actual)
	} //if

	// Get the values
	expecVals, actualVals := expected.Query(), actual.Query()

	// Loop through the expected values
	for k, v := range expecVals {
		// Get the value
		av, ok := actualVals[k]

		// Make sure the key was present
		if !ok {
			t.Errorf("No key %s", k)
			continue
		} //if

		// Make sure the values are the same
		if av[0] != v[0] {
			t.Errorf(`Expected value to be "%s" but was "%s"`, v, av)
		} //if
	} //for
} //func

func TestCurrenciesReq(t *testing.T) {
	// Create a test web server
	srv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte(`[
    {
        "id": "bitcoin",
        "name": "Bitcoin",
        "symbol": "BTC",
        "rank": "1",
        "price_usd": "573.137",
        "price_btc": "1.0",
        "24h_volume_usd": "72855700.0",
        "market_cap_usd": "9080883500.0",
        "available_supply": "15844176.0",
        "total_supply": "15844176.0",
        "percent_change_1h": "0.04",
        "percent_change_24h": "-0.3",
        "percent_change_7d": "-0.57",
        "last_updated": "1472762067"
    },
    {
        "id": "ethereum",
        "name": "Ethereum",
        "symbol": "ETH",
        "rank": "2",
        "price_usd": "12.1844",
        "price_btc": "0.021262",
        "24h_volume_usd": "24085900.0",
        "market_cap_usd": "1018098455.0",
        "available_supply": "83557537.0",
        "total_supply": "83557537.0",
        "percent_change_1h": "-0.58",
        "percent_change_24h": "6.34",
        "percent_change_7d": "8.59",
        "last_updated": "1472762062"
    }
]      `))
	}))

	// Stop the server at function exit
	defer srv.Close()

	// Make the request
	currencies, err := currenciesReq(srv.URL)

	// Check for an error
	if err != nil {
		t.Fatal("Unexpected error:", err)
	} //if

	// Make sure there were two currencies
	if expected, actual := 2, len(currencies); actual != expected {
		t.Errorf("Expected %d currencies but there were %d", expected, actual)
	} //if
} //func

func TestMakeCurrencyURL(t *testing.T) {
	expected := "https://api.coinmarketcap.com/v1/ticker/a?currency=b"
	actual := makeCurrencyURL("a", "b")

	if actual != expected {
		t.Errorf(`Expected url to be "%s" but was "%s"`, expected, actual)
	} //if
} //func

func TestCurrencyRequest(t *testing.T) {
	// Create the web server
	srv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte(`[
    {
        "id": "bitcoin",
        "name": "Bitcoin",
        "symbol": "BTC",
        "rank": "1",
        "price_usd": "573.137",
        "price_btc": "1.0",
        "24h_volume_usd": "72855700.0",
        "market_cap_usd": "9080883500.0",
        "available_supply": "15844176.0",
        "total_supply": "15844176.0",
        "max_supply": "21000000.0",
        "percent_change_1h": "0.04",
        "percent_change_24h": "-0.3",
        "percent_change_7d": "-0.57",
        "last_updated": "1472762067"
    }
]`))
	}))

	// Stop the server at function exit
	defer srv.Close()

	// Make the request
	currency, err := currencyReq(srv.URL)

	// Check for an error
	if err != nil {
		t.Fatal("Unexpected error:", err)
	} //if

	// Check the id
	if expected, actual := "bitcoin", currency.ID; actual != expected {
		t.Errorf(`Expected id to be "%s" but was "%s"`, expected, actual)
	} //if
} //func

func TestGlobalRequest(t *testing.T) {
	// Create the server
	srv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte(`{
    "total_market_cap_usd": 201241796675,
    "total_24h_volume_usd": 4548680009,
    "bitcoin_percentage_of_market_cap": 62.54,
    "active_currencies": 896,
    "active_assets": 360,
    "active_markets": 6439,
    "last_updated": 1509909852
}  `))
	}))

	// Stop the server at function exit
	defer srv.Close()

	// Make the request
	info, err := globalReq(srv.URL)

	// Check for an error
	if err != nil {
		t.Fatal("Unexpected error:", err)
	} //if

	// Check the active currencies
	if expected, actual := 896, info.ActiveCurrencies; actual != expected {
		t.Errorf("Expected active currencies to be %d but eas %d", expected, actual)
	} //if
} //func
