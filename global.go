package coinmarketcap

// Global for crypto currencies.
type Global struct {
	TotalMarketCapUSD   int     `json:"total_market_cap_usd"`
	TotalDailyVolumeUSD int     `json:"total_24h_volume_usd"`
	BTCPercentMarketCap float32 `json:"bitcoin_percentage_of_market_cap"`
	ActiveCurrencies    int     `json:"active_currencies"`
	ActiveAssets        int     `json:"active_assets"`
	ActiveMarkets       int     `json:"active_markets"`
	LastUpdate          int64   `json:"last_updated"`
} //struct
