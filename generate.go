package coinmarketcap

//go:generate go run cmd/generators/currency.go -out currency.go -package coinmarketcap AUD BRL CAD CHF CLP CNY CZK DKK EUR GBP HKD HUF IDR ILS INR JPY KRW MXN MYR NOK NZD PHP PKR PLN RUB SEK SGD THB TRY TWD ZAR
//go:generate go run cmd/generators/nationalcurrencies.go -out nationalcurrencies.go AUD BRL CAD CHF CLP CNY CZK DKK EUR GBP HKD HUF IDR ILS INR JPY KRW MXN MYR NOK NZD PHP PKR PLN RUB SEK SGD THB TRY TWD ZAR
