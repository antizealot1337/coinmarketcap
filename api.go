package coinmarketcap

import (
	"encoding/json"
	"net/http"
	"net/url"
	"path"
	"strconv"
)

func coinmarketcap(query url.Values, pathParts ...string) string {
	// Areate the url
	u := url.URL{
		Scheme:   "https",
		Host:     "api.coinmarketcap.com",
		Path:     path.Join(pathParts...),
		RawQuery: query.Encode(),
	}
	return u.String()
} //func

func makeCurrenciesURL(start, count int, currency string) string {
	// Create the values for the quety
	values := make(url.Values, 3)

	// Add the start
	values.Add("start", strconv.Itoa(start))

	// Add the limit
	values.Add("limit", strconv.Itoa(count))

	// Add the currency
	values.Add("currency", currency)
	return coinmarketcap(values, "v1", "ticker")
} //func

func currenciesReq(url string) (currencies []Currency, err error) {
	// Make the request
	resp, err := http.Get(url)

	// Check for an error
	if err != nil {
		return nil, err
	} //if

	// Make sure the body is closed at the end of the request
	defer resp.Body.Close()

	// Create the decoder
	decoder := json.NewDecoder(resp.Body)

	// Decode the currencies
	err = decoder.Decode(&currencies)
	return
} //func

// GetCurrencies from coinmarketcap info from "https://api.coinmarketcap.com/".
func GetCurrencies(start, count int, currency string) ([]Currency, error) {
	// Make the request
	return currenciesReq(makeCurrenciesURL(start, count, currency))
} //GetCurrencies

func makeCurrencyURL(name, currency string) string {
	// Create the values for the quety
	values := make(url.Values, 1)

	// Add the currency
	values.Add("currency", currency)

	return coinmarketcap(values, "v1", "ticker", name)
} //func

func currencyReq(url string) (currency Currency, err error) {
	// Make the request
	resp, err := http.Get(url)
	if err != nil {
		return
	} //if
	defer resp.Body.Close()

	// Create the decoder
	decoder := json.NewDecoder(resp.Body)

	// The currencies
	var currencies []Currency

	// Decode the currency
	err = decoder.Decode(&currencies)

	if err == nil {
		currency = currencies[0]
	} //if
	return
} //func

// GetCurrency returns information about a particular cryto currency.
func GetCurrency(name, currency string) (Currency, error) {
	// Make the request
	return currencyReq(makeCurrencyURL(name, currency))
} //func

func makeGlobalReq(currency string) string {
	// Create the values
	values := make(url.Values)

	// Set the currency
	values.Add("convert", currency)

	// Make the URL
	return coinmarketcap(values, "v1", "global")
} //func

func globalReq(url string) (info Global, err error) {
	// Make the request
	resp, err := http.Get(url)
	if err != nil {
		return
	} //if
	defer resp.Body.Close()

	// Create the decoder
	decoder := json.NewDecoder(resp.Body)

	// Decode the currency
	err = decoder.Decode(&info)

	return
} //func

// GetGlobal returns global data.
func GetGlobal(currency string) (info Global, err error) {
	return globalReq(makeGlobalReq(currency))
} //fnc
