package coinmarketcap

// Percent indicates a percentage change.
type Percent string

// Loss indictes if the percent was a loss. It detects a leading '-' to make its
// determination.
func (p Percent) Loss() bool {
	if p == "" {
		return true
	} //if
	return p[0] == '-'
} //func

func (p Percent) String() string {
	return string(p) + "%"
} //func
