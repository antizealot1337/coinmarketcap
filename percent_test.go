package coinmarketcap

import "testing"

func TestPercentLoss(t *testing.T) {
	// Check empty
	if !Percent("").Loss() {
		t.Error("Expected empty string is not a loss")
	} //if

	// Check negative
	if !Percent("-1").Loss() {
		t.Error("Expected loss for string with leading \"-\"")
	} //if

	// Check other
	if Percent("1").Loss() {
		t.Error("Expected not loss for string without leading \"-\"")
	} //if
} //func

func TestPercentString(t *testing.T) {
	if expected, actual := "1%", Percent("1").String(); actual != expected {
		t.Errorf(`Expected the string to be "%s" but was "%s"`, expected, actual)
	} //if
} //func
